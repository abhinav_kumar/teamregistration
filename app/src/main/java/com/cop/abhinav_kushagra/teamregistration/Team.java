package com.cop.abhinav_kushagra.teamregistration;

import java.io.Serializable;
import java.util.ArrayList;


public class Team implements Serializable{
    private String teamName;
    private ArrayList<User> teamMember;

    public Team(String teamName){
        this.teamName = teamName;
        this.teamMember = new ArrayList<User>();
    }

    public String getTeamName(){
        return teamName;
    }

    public ArrayList<User> getTeamMember(){
        return teamMember;
    }

    public void addTeamMember(User user){
        teamMember.add(user);
    }

    public void removeTeamMember(){
        if(teamMember.size()>0)
            teamMember.remove(teamMember.size()-1);
    }
    public String showTeamMember(){
        return teamMember.get(0).getName()+" "+teamMember.get(1).getName()+" "+teamMember.get(0).getEntryNo()+" "+teamMember.get(1).getEntryNo();
    }

}

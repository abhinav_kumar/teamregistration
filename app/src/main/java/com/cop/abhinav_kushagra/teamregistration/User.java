package com.cop.abhinav_kushagra.teamregistration;

import java.io.Serializable;


public class User implements Serializable{
    private String entryNo;
    private String name;

    public User(String entryNo,String name){
        this.entryNo=entryNo;
        this.name=name;
    }

    public String getName(){
        return name;
    }

    public String getEntryNo(){
        return entryNo;
    }
}

package com.cop.abhinav_kushagra.teamregistration;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    EditText teamName;
    Button next;
    Team team;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        next = (Button) findViewById(R.id.next );
        teamName = (EditText) findViewById(R.id.teamName);

    }
    /**
     * This function sends the team name to next activity.
     *
     * @since   2015-01-24
     */
    public void sendTeamName( View view ){
        Intent intent = new Intent(this , Team1.class );
        team = new Team(teamName.getText().toString());
        intent.putExtra(getResources().getString(R.string.parsing_message),team);
        startActivity(intent);
        overridePendingTransition(R.anim.right_in, R.anim.left_out);

    }
}

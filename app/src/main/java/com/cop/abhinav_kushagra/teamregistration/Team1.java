package com.cop.abhinav_kushagra.teamregistration;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class Team1 extends AppCompatActivity {

    EditText name,entryNo;
    Button next,skip;
    RelativeLayout rel;
    TextView memberNumb;
    int membercount=1 ;
    Animation animation;
    Team team;

    @Override
    /**
     * Activity initialisation function
     *
     * @since   2015-01-24
     */
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team1);

        rel = (RelativeLayout) findViewById(R.id.rel);
        memberNumb = (TextView) findViewById(R.id.memberNo);
        next = (Button) findViewById(R.id.next);
        skip = (Button) findViewById(R.id.skip);
        skip.setVisibility(View.GONE);
        entryNo = (EditText) findViewById(R.id.entryNo);
        name = (EditText) findViewById(R.id.personName);
        animation = AnimationUtils.loadAnimation(this, R.anim.shake);
        Intent intent = getIntent();
        team=(Team) intent.getSerializableExtra(getResources().getString(R.string.parsing_message));
    }

    /**
     * Function to change the team member number in the Team1 Activity.
     *
     * This function also manages the change of background color.
     *
     * @since   2015-01-24
     */
    public void changeItem(View view){

        // if already 3 members, pass the data to next activity
        if (membercount==3){
            if(checkValidity(entryNo.getText().toString().trim()) && !(name.getText().toString().trim().matches(""))) {
                team.addTeamMember(new User(entryNo.getText().toString(), name.getText().toString()));
                skip.performClick();
            }
            else {
                membercount--;
                if (!(checkValidity(entryNo.getText().toString().trim()))){
                    entryNo.startAnimation(animation);
                    entryNo.setError("Invalid Entry Number");
                }
                if((name.getText().toString().trim().matches(""))){
                    name.startAnimation(animation);
                    name.setError("Name can not be empty");
                }

            }

        }
        //else check if entry number is valid and then add it to the message queue
        else {
            if (checkValidity(entryNo.getText().toString().trim()) && !(name.getText().toString().trim().matches("")) ){
                team.addTeamMember(new User(entryNo.getText().toString(),name.getText().toString()));
                name.setText("");
                entryNo.setText("");

                if (membercount==2) {
                    skip.setVisibility(View.VISIBLE);
                    rel.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    memberNumb.setText("TEAM MEMBER 3");
                }
                else {
                    rel.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                    memberNumb.setText("TEAM MEMBER 2");
                }
            }
            else{
                membercount--;
                if (!(checkValidity(entryNo.getText().toString().trim()))){
                    entryNo.startAnimation(animation);
                    entryNo.setError("Invalid Entry Number");
                }
                if((name.getText().toString().trim().matches(""))){
                    name.startAnimation(animation);
                    name.setError("Name can not be empty");
                }
            }
        }

        // increment only if there is no error
        membercount++;

    }

    /**
     * Function to move to next activity (Register).
     *
     * @since   2015-01-24
     */
    public void goToRegister(View view){
        if(membercount==3){
            team.addTeamMember(new User("",""));
        }
        Intent intent = new Intent(this , Register.class );
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(getResources().getString(R.string.parsing_message), team);
        startActivity(intent);
        overridePendingTransition(R.anim.right_in, R.anim.left_out);
        finish();
    }

    /**
     * This function checks if the entry number is valid.
     *
     * @since   2015-01-24
     */
    public boolean checkValidity(String entryNo){
        return entryNo.matches("[2][0][01][0-9][a-zA-Z]{2}[a-zA-Z0-9][0-9]{4}");
    }


    @Override
    /**
     * This function manages the data in case back button is pressed.
     *
     * @since   2015-01-24
     */
    public void onBackPressed() {
        membercount--;
        if(membercount>0){
            if(membercount==1){
                rel.setBackgroundColor(getResources().getColor(R.color.grey));
                memberNumb.setText("TEAM MEMBER 1");
                entryNo.setText(team.getTeamMember().get(0).getEntryNo());
                name.setText(team.getTeamMember().get(0).getName());
                team.removeTeamMember();
            }else {
                rel.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                memberNumb.setText("TEAM MEMBER 2");
                entryNo.setText(team.getTeamMember().get(1).getEntryNo());
                name.setText(team.getTeamMember().get(1).getName());
                team.removeTeamMember();
            }
        }else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("check","onResume"+membercount+team.getTeamMember().size());
    }
}

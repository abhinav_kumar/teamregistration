package com.cop.abhinav_kushagra.teamregistration;

import android.app.DownloadManager;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Register extends AppCompatActivity {
    TextView responseView;
    Button goToHome,exit,tryAgain;
    Team team;

    @Override

    /**
     * Function to initialise the "Register" activity.
     *
     * @since   2015-01-24
     */
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        Log.d("onCreate", "Register");
        goToHome = (Button) findViewById(R.id.goToHome);
        exit = (Button) findViewById(R.id.exit);
        tryAgain = (Button) findViewById(R.id.tryAgain);
        responseView = (TextView) findViewById(R.id.response);
        Intent intent = getIntent();
        team=(Team) intent.getSerializableExtra(getResources().getString(R.string.parsing_message));
        Log.d("teaminfo",team.showTeamMember());
        registerTeam();

        tryAgain.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                registerTeam();
            }
        });

        exit.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    /**
     * Function to go back to MainActivity. Used by Go Home button.
     *
     * @since   2015-01-24
     */
    public void gotoHome(View view){
        Intent intent = new Intent(this,MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }


    @Override
    public void onBackPressed() {
    }


    /**
     * This function sends the volley request to the server and behaves according to the provided response
     *
     * @since   2015-01-24
     */
    public void registerTeam(){
        // Instantiate the RequestQueue.
        Log.d("registerReached", "registerReached"+team.showTeamMember());
        RequestQueue queue = Volley.newRequestQueue(this);
        Log.d("register function", team.getTeamMember().toString()+team.getTeamMember().size());
        String url ="http://agni.iitd.ernet.in/cop290/assign0/register/";


        // This code sends volley request and listens to response

        StringRequest sr= new StringRequest(Request.Method.POST,url,new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                tryAgain.setVisibility(View.GONE);
                JSONObject responseText;
                int responseCode=3;
                String responseMessage="";
                try{
                    responseText=new JSONObject(response);
                    responseCode=responseText.getInt("RESPONSE_SUCCESS");
                    responseMessage=responseText.getString("RESPONSE_MESSAGE");
                    Log.d("responseJSON", responseCode+responseMessage);

                }catch (Throwable t){

                }
                //behave according to the recieved response code
                if(responseCode==0){
                    if(responseMessage.equals("Data not posted!")){
                        responseView.setText("Not Registered !!!" + " Data Not Posted\n ");
                    }
                    else if (responseMessage.equals("User already registered")){
                        responseView.setText("User Already Registered !!!" + " Data Not Posted\n " );
                    }
                    else{
                        responseView.setText("Unknown Error!!!" + " Data Not Posted\n " );
                    }
                }
                else{
                    responseView.setText("Registered !!!");
                }
                Log.d("response", response.toString());
            }
        },new Response.ErrorListener() {
            @Override
            //in case of some network error
            public void onErrorResponse(VolleyError error) {
                responseView.setText("Network Error");
                Log.d("responseError","C"+error.toString());
                tryAgain.setVisibility(View.VISIBLE);
            }
        }){
            @Override

            // To provide the required input
            protected Map<String,String> getParams(){
                final HashMap<String, String> params = new HashMap<String, String>();
                params.put("teamname",""+ team.getTeamName()+"");
                params.put("entry1",""+  team.getTeamMember().get(0).getEntryNo()+"");
                params.put("name1", ""+  team.getTeamMember().get(0).getName()+"");
                params.put("entry2", ""+  team.getTeamMember().get(1).getEntryNo()+"");
                params.put("name2", ""+  team.getTeamMember().get(1).getName()+"");
                if(team.getTeamMember().size()==2){
                    params.put("entry3","");
                    params.put("name3","");
                }
                else{
                    params.put("entry3", ""+  team.getTeamMember().get(2).getEntryNo()+"");
                    params.put("name3", "" +  team.getTeamMember().get(2).getName() + "");
                }
                Log.d("params", params.toString());
                return params;
            }

        };

        queue.add(sr);

    }

}
